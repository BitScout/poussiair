# PoussiAir

poussière {f} : dust

Yes, it's a french pun.

Measure air quality (fine dust) on the go. The bar charts on the top are not linear but to the power of .8 to display high values while also seeing differences in lower pollution environments.

![alt text](images/display_low.jpg "Photo from low pollution environment")

       
                                               +-----+
                                  +------------+ USB +------------+
                                  |            +-----+            |
                                  | [ ]D13/SCK        MISO/D12[ ] |
                                  | [ ]3.3V           MOSI/D11[ ]~|
                                  | [ ]V.ref     ___    SS/D10[ ]~|
                                  | [ ]A0       / N \       D9[ ]~|
                                  | [ ]A1      /  A  \      D8[ ] |
     +------------+               | [ ]A2      \  N  /      D7[ ] |
     |            |               | [ ]A3       \_0_/       D6[ ]~|
     | O  SDA [+---------------------+]A4/SDA               D5[ ]~|
     | L  SCL [<---------------------+]A5/SCL               D4[ ] |
     | E  VCC [<--------------+   | [ ]A6              INT1/D3[ ]~|
     | D  GND [+----------+   |   | [ ]A7              INT0/D2[ ] |
     |            |       |   +------+]5V                  GND[+-------------+
     +------------+       |   |   | [ ]RST                 RST[ ] |          |
                          +----------+]GND   5V MOSI GND   TX1[+-----------+ |         +------------+
                              |   | [ ]Vin   [ ] [ ] [ ]   RX1[+---------+ | |         |            |
                              |   |          [ ] [ ] [ ]          |      | | |         | [ ]      S |
                              |   |          MISO SCK RST         |      | | |         | [ ]      D |
                              |   | NANO+V3                       |   +-------------------+] 5V   S |
                              |   +-------------------------------+   |  | | |         | [ ]      0 |
                              |                                       |  | | +------------+] GND  1 |
                              |                                       |  | +-------------->] RX   1 |
                              |                                       |  +----------------+] TX     |
                              +---------------------------------------+                |            |
                                                                                       +------------+
