
// Display -> Arduino Nano
// -----------------------
// GND -> GND
// VCC -> 5V
// SCL -> A5
// SDA -> A4

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define FIRST_BLUE_LINE 8
#define LAST_LINE 31
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define LEN 9
#define windowSize 10
#define HISTORY 22

float scalePower = .8;
float historyPower = .55;
int   scaleInterval = 7;

float aveArr[windowSize];
int loopcnt = 0;
unsigned char incomingByte = 0; // for incoming serial data
unsigned char buf[LEN];
int PM2_5Val = 0;
int PM10Val = 0;
int PM2_5ave = 0;
int pm10history[HISTORY];
int pm2history[HISTORY];
int historyPointer = 0;

void setup() {
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextColor(WHITE);
  display.clearDisplay();
  printLevel(0, 0);
  display.display();
}

void loop() {
  int i;
  unsigned char checksum;

  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (incomingByte == 0xAA) {
      Serial.readBytes(buf, LEN);
      if ((buf[0] == 0xC0) && (buf[8] == 0xAB)) {
        for (i = 1; i <= 6; i++) {
          checksum = checksum + buf[i];
        }
        if (checksum == buf[7]) {
          PM2_5Val = ((buf[2] << 8) + buf[1]) / 10;
          PM10Val = ((buf[4] << 8) + buf[3]) / 10;
          PM2_5ave = runAve(PM2_5Val, windowSize);

          pm10history[historyPointer] = PM10Val;
          pm2history[historyPointer] = PM2_5Val;

          display.clearDisplay();
          printLevel(PM10Val, PM2_5Val);
          printHistory();
          display.display();
          
          historyPointer = (historyPointer + 1) % HISTORY;
        }
        loopcnt++;
      }
    }
  }
}

void printLevel(int pm10, int pm2) {
  display.drawLine(0, 0, 0, FIRST_BLUE_LINE - 1, WHITE);
  int yLowerBar = (FIRST_BLUE_LINE) / 2;

  for (int x = 1; x < pow(pm10, scalePower); x++) {
    if (0 == x % scaleInterval) {
      display.drawLine(x, 0, x, yLowerBar - 1, WHITE);
    } else {
      display.drawLine(x, 1, x, yLowerBar - 2, WHITE);
    }
  }
  for (int x = 1; x < pow(pm2, scalePower); x++) {
    if (0 == x % scaleInterval) {
      display.drawLine(x, yLowerBar, x, FIRST_BLUE_LINE - 1, WHITE);
    } else {
      display.drawLine(x, yLowerBar + 1, x, FIRST_BLUE_LINE - 2, WHITE);
    }
  }

  display.setCursor(0, FIRST_BLUE_LINE + 7);
  display.setTextSize(2);
  display.print(pm10);
  display.setCursor(92, FIRST_BLUE_LINE + 7);
  if(pm2 < 10) display.print(" ");
  if(pm2 < 100) display.print(" ");
  display.print(pm2);
}

void printHistory() {
  int arrayPos = 0;
  int val10;
  int val2;
  int y;
  
  for (int i = 0; i < HISTORY; i++) {
    arrayPos = (historyPointer + i)  % HISTORY;
    val10    = pow(pm10history[arrayPos], historyPower);
    val2     = pow(pm2history[arrayPos], historyPower);
    y        = LAST_LINE + 1 - i;
    
    display.drawLine(65, y, 65 + val2, y, WHITE);
    display.drawLine(63, y, 63 - val10, y, WHITE);
  }
}

float runAve(float x, int n) {
  float sum;
  int i;

  for (i = 0; i < n - 1; i++) {
    aveArr[i] = aveArr[i + 1];
  }
  aveArr[n - 1] = x;
  for (i = 0; i < n; i++) {
    sum = sum + aveArr[i];
  }
  sum = sum / n;

  return sum;
}

